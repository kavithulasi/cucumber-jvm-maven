Feature: Google Search
  As a user
  I want to google specific term
  
  Scenario: Search for the term cucumber and click the first link from the search result
   Given the user has to search a term
   And the user enters the search term "cucumber" 
    Then the search results are displayed
    