package com.falcon.bdd.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = { "pretty", "html:target/cucumber" },
        glue = "com.falcon.bdd.steps",
        features = {"classpath:cucumber/calculator.feature", 
        		"classpath:cucumber/google.feature",
        		"classpath:cucumber/testrestapi.feature"
        }
)
public class RunCalculatorTest {
}
