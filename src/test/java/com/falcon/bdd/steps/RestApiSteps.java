package com.falcon.bdd.steps;

import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import static com.jayway.restassured.RestAssured.given;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class RestApiSteps {

	
	public static WebDriver driver;
	Logger logger = Logger.getLogger(GoogleSearchSteps.class.getName());
	@Given("^the user has to check if Google is up$")
	public void the_user_has_to_check_if_Google_is_up() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   logger.info("This test is to check if google is up");
	}

	@Given("^user navigates to the rest end point$")
	public void user_navigates_to_the_rest_end_point() throws Throwable {
		given().when().get("http://www.google.com").then().statusCode(200);
	    
	}

	@Then("^verify google is up$")
	public void verify_google_is_up() throws Throwable {
	    logger.info("Google is up");
	    
	}


	
}
