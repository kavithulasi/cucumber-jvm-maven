package com.falcon.bdd.steps;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;


public class GoogleSearchSteps {

	public static WebDriver driver;
	Logger logger = Logger.getLogger(GoogleSearchSteps.class.getName());

	@Given("^the user has to search a term$")
	public void the_user_has_to_search_a_term() throws Throwable {
				
	//	String file = getClass().getResource("/drivers/geckodriver.exe").getFile();
		String file = getClass().getResource("/drivers/chromedriver.exe").getFile();
		InputStream in = getClass().getResourceAsStream("/properties/driver.properties");
		Properties prop = new Properties();
		prop.load(in);
		String environment = prop.getProperty("run");
		System.out.println("The environment is" + environment);
		if (environment.equals("local"))
		{
			System.setProperty("webdriver.gecko.driver", file);
			//System.setProperty("webdriver.chrome.driver", file);
			driver = new ChromeDriver();
			//driver = new FirefoxDriver();
			driver.get("https://google.com");
			//driver = new FirefoxDriver();
			
			
		}
		
		else if (environment.equals("server")){
			
			//System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
			
		//System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
			
			String baseURL = "https://google.com";
			String hubURL = "http://ec2-54-163-136-164.compute-1.amazonaws.com:4444/wd/hub";
		    DesiredCapabilities capability = DesiredCapabilities.firefox();
		    capability.setPlatform(Platform.LINUX);
		    driver = new RemoteWebDriver(new URL(hubURL) , capability);
		    driver.get(baseURL);
		
		
		
		}
		

		

	}

	@Given("^the user enters the search term \"(.*?)\"$")
	public void the_user_enters_the_search_term(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		WebDriverWait wait = new WebDriverWait(driver, 10);

		WebElement googleSearch = wait.until(ExpectedConditions.elementToBeClickable(By.id("lst-ib")));
		googleSearch.sendKeys(arg1);
		googleSearch.sendKeys(Keys.ENTER);
		
	}

	@Then("^the search results are displayed$")
	public void the_search_results_are_displayed() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		driver.quit();
	}

}
